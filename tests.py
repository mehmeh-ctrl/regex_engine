import unittest
from typing import List, Tuple, Any, Union
from regex_engine import (find_previous_character_if_question_mark_metacharacter,
                          find_previous_character_if_multiplication_mark_metacharacter,
                          find_previous_character_if_addition_mark,
                          single_character_comparison,
                          find_next_character_if_backslash_detected,
                          single_character_comparison_without_wildcard,
                          recursive_regex,
                          unequal_strings_find_match)

from to_tests import (make_comparisons_question_mark,
                      find_previous_character_if_question_mark_metacharacter_reversed,
                      make_comparisons_question_mark_reversed,
                      make_comparisons_multiplication_mark,
                      make_comparisons_addition_mark,
                      find_next_character_if_backslash_detected_reversed)


class RegexEngineTests(unittest.TestCase):

    def test_find_previous_character_if_question_mark_metacharacter(self):

        question_mark_metacharacter_input_list: List[str] = ["colou?r",
                                                             "colou*r",
                                                             ".?",
                                                             "\\?",
                                                             "colou\\?r",
                                                             "a",
                                                             ""]

        question_mark_metacharacter_output_list: List[Tuple[int, str]] = [(4, "u"),
                                                                          (-1, "s"),
                                                                          (0, "."),
                                                                          (0, "\\"),
                                                                          (5, "\\"),
                                                                          (-1, "s"),
                                                                          (-1, "s")]

        i: int

        for i in range(0, len(question_mark_metacharacter_input_list)):
            self.assertEqual(find_previous_character_if_question_mark_metacharacter(
                question_mark_metacharacter_input_list[i], 0
            ), question_mark_metacharacter_output_list[i])

    def test_make_comparisons_question_mark(self):
        make_comparisons_question_mark_input_list: List[Tuple[str, str]] = [("colou?r", "color"),
                                                                            ("colou?r", "colour"),
                                                                            ("colou?r", "colouur"),
                                                                            ("colou*r", "color"),
                                                                            (".?", "aaa"),
                                                                            ("a", "a"),
                                                                            ("", "")]

        make_comparisons_question_mark_output_list: List[Tuple[bool, int]] = [(True, 0),
                                                                              (True, 1),
                                                                              (True, 1),
                                                                              (False, 0),
                                                                              (True, 2),
                                                                              (False, 0),
                                                                              (False, 0)]

        i: int

        for i in range(0, len(make_comparisons_question_mark_input_list)):
            self.assertEqual(make_comparisons_question_mark(*make_comparisons_question_mark_input_list[i]),
                             make_comparisons_question_mark_output_list[i])

    def test_find_previous_character_if_question_mark_metacharacter_reversed(self):

        regexes_reversed_input_list: List[str] = ["r?\\uoloc",
                                                  "?\\",
                                                  "?.",
                                                  "+.",
                                                  "a",
                                                  ""]

        regexes_reversed_output_list: List[Tuple[int, str]] = [(2, "\\"),
                                                               (1, "\\"),
                                                               (1, "."),
                                                               (-1, "s"),
                                                               (-1, "s"),
                                                               (-1, "s")]

        i: int

        for i in range(0, len(regexes_reversed_input_list)):
            self.assertEqual(
                find_previous_character_if_question_mark_metacharacter_reversed(
                    regexes_reversed_input_list[i], 0),
                regexes_reversed_output_list[i])

    def test_make_comparisons_question_mark_reversed(self):

        pre_reversed_input_list: List[Tuple[str, str]] = [(".?", "aaa"),
                                                          ("colou?r", "colour"),
                                                          ("colou?r", "colouur"),
                                                          ("colou?r", "color")]

        post_reversed_output_list: List[Tuple[bool, int]] = [(True, 2),
                                                             (True, 1),
                                                             (False, 2),
                                                             (True, 0)]

        post_reversed_input_list: List[Tuple[str, str]] = []

        _: int
        regex_tuple: Tuple[str, str]

        for _, regex_tuple in enumerate(pre_reversed_input_list):
            regex_reversed: str = regex_tuple[0][::-1]
            input_str_reversed: str = regex_tuple[1][::-1]
            post_reversed_input_list.append((regex_reversed, input_str_reversed))

        i: int

        for i in range(0, len(post_reversed_input_list)):
            self.assertEqual(make_comparisons_question_mark_reversed(*post_reversed_input_list[i]),
                             post_reversed_output_list[i])

    def test_find_previous_character_if_multiplication_mark_metacharacter(self):
        multiplication_mark_metacharacter_input_list: List[str] = ["a",
                                                                   "",
                                                                   "colou?r",
                                                                   "colou*r",
                                                                   ".*"]

        multiplication_mark_metacharacter_output_list: List[Tuple[int, str]] = [
            (-1, 's'),
            (-1, 's'),
            (-1, 's'),
            (4, 'u'),
            (0, '.')
        ]

        _: int
        mult_regex: str

        for _, mult_regex in enumerate(multiplication_mark_metacharacter_input_list):
            self.assertEqual(find_previous_character_if_multiplication_mark_metacharacter(mult_regex, 0),
                             multiplication_mark_metacharacter_output_list[_])

    def test_make_comparisons_multiplication_mark(self):
        multiplication_mark_input_list: List[Tuple[str, str]] = [("colou*r", "color"),
                                                                 ("colou*r", "colour"),
                                                                 ("colou*r", "colouur"),
                                                                 (".*", "aaa"),
                                                                 (".*", "aa"),
                                                                 (".*", "a"),
                                                                 (".*", "")]

        multiplication_mark_output_list: List[Tuple[bool, int]] = [(True, 0),
                                                                   (True, 1),
                                                                   (True, 2),
                                                                   (True, 2),
                                                                   (True, 2),
                                                                   (True, 1),
                                                                   (True, 0)]

        _: int
        regex_tuple: Tuple[str, str]

        for _, regex_tuple in enumerate(multiplication_mark_input_list):
            self.assertEqual(make_comparisons_multiplication_mark(*regex_tuple),
                             multiplication_mark_output_list[_])

    def test_find_previous_character_if_addition_mark(self):

        addition_mark_input_list: List[str] = ["",
                                               "a",
                                               "colou\\?r",
                                               "3\\+3",
                                               "^n.+p$",
                                               "^n.+pe$",
                                               "^no+pe$",
                                               "^no+",
                                               "no+$",
                                               "colou+r",
                                               ".+"]

        addition_mark_output_list: List[Union[Tuple[int, str], Any]] = [(-1, 's'),
                                                                        (-1, 's'),
                                                                        (-1, 's'),
                                                                        (1, '\\'),
                                                                        (2, '.'),
                                                                        (2, '.'),
                                                                        (2, 'o'),
                                                                        (2, 'o'),
                                                                        (1, 'o'),
                                                                        (4, 'u'),
                                                                        (0, '.')]

        _: int
        regex: str

        for _, regex in enumerate(addition_mark_input_list):
            self.assertEqual(find_previous_character_if_addition_mark(regex, 0), addition_mark_output_list[_])

    def test_make_comparisons_addition_mark(self):
        addition_mark_input_list: List[Tuple[str, str]] = [("^n.+p$", "noooooooope"),
                                                           ("^n.+pe$", "noooooooope"),
                                                           ("^no+", "noooooooope"),
                                                           ("^no+pe$", "noooooooope"),
                                                           ("no+$", "noooooooope"),
                                                           (".+", "aaa"),
                                                           ("colou+r", "color"),
                                                           ("colou+r", "colour")]

        addition_mark_output_list: List[Tuple[bool, int]] = [(True, 2),
                                                             (True, 2),
                                                             (True, 2),
                                                             (True, 2),
                                                             (True, 2),
                                                             (True, 2),
                                                             (False, 0),
                                                             (True, 1)]

        _: int
        regex_tuple: Tuple[str, str]

        for _, regex_tuple in enumerate(addition_mark_input_list):
            self.assertEqual(make_comparisons_addition_mark(*regex_tuple),
                             addition_mark_output_list[_])

    def test_single_character_comparison(self):

        single_character_comparison_input: List[str] = ["a|a",
                                                        "a|b",
                                                        "7|7",
                                                        "6|7",
                                                        ".|a",
                                                        "a|.",
                                                        "|a",
                                                        "|",
                                                        "a|"]

        single_character_comparison_output: List[bool] = [True,
                                                          False,
                                                          True,
                                                          False,
                                                          True,
                                                          False,
                                                          True,
                                                          True,
                                                          False]

        _: int
        regex_pair: str

        for _, regex_pair in enumerate(single_character_comparison_input):
            self.assertEqual(single_character_comparison(regex_pair), single_character_comparison_output[_])

    def test_find_next_character_if_backslash_detected(self):
        backslash_next_input_list: List[str] = ["", "a", "\\\\", "colou\\?r",
                                                "\\?", "3\\+3", "\\.$"]

        backslash_next_output_list: List[Tuple[int, str]] = [(-1, 's'),
                                                             (-1, 's'),
                                                             (1, '\\'),
                                                             (6, '?'),
                                                             (1, '?'),
                                                             (2, '+'),
                                                             (1, '.')]

        for _, regex in enumerate(backslash_next_input_list):
            self.assertEqual(find_next_character_if_backslash_detected(regex, 0),
                             backslash_next_output_list[_])

    def test_find_previous_character_if_backslash_detected_reversed(self):

        backslash_pre_reversed_input_list: List[str] = ["", "a", "\\\\", "colou\\?r",
                                                        "\\?", "3\\+3", "\\.$"]
        backslash_reversed_output_list = [(-1, 's'),
                                          (-1, 's'),
                                          (1, '\\'),
                                          (1, '?'),
                                          (0, '?'),
                                          (1, '+'),
                                          (1, '.')]

        backslash_post_reversed_input_list: List[str] = []

        _: int
        regex: str
        for _, regex in enumerate(backslash_pre_reversed_input_list):
            regex_reversed: str = regex[::-1]
            backslash_post_reversed_input_list.append(regex_reversed)

        for _, regex_reversed in enumerate(backslash_post_reversed_input_list):
            self.assertEqual(find_next_character_if_backslash_detected_reversed(regex_reversed, 0),
                             backslash_reversed_output_list[_])

    def test_single_character_comparison_without_wildcard(self):

        single_character_comparison_input: List[str] = ["a|a",
                                                        "a|b",
                                                        "7|7",
                                                        "6|7",
                                                        ".|a",
                                                        "a|.",
                                                        "|a",
                                                        "|",
                                                        "a|"]

        single_character_comparison_without_wildcard_output_list: List[bool] = [
            True, False, True, False, False, False, True, True, False
        ]

        _: int
        regex_pair: str

        for _, regex_pair in enumerate(single_character_comparison_input):
            self.assertEqual(single_character_comparison_without_wildcard(regex_pair),
                             single_character_comparison_without_wildcard_output_list[_])

    def test_recursive_regex(self):

        recursive_regex_input_list: List[str] = ["a|a",
                                                 "a|b",
                                                 "7|7",
                                                 "6|7",
                                                 ".|a",
                                                 "a|.",
                                                 "|a",
                                                 "|",
                                                 "a|",
                                                 "apple|apple",
                                                 ".pple|apple",
                                                 "appl.|apple",
                                                 ".....|apple",
                                                 "|apple",
                                                 "apple|",
                                                 "apple|peach"]

        recursive_regex_output_list: List[bool] = [True,
                                                   False,
                                                   True,
                                                   False,
                                                   True,
                                                   False,
                                                   True,
                                                   True,
                                                   False,
                                                   True,
                                                   True,
                                                   True,
                                                   True,
                                                   True,
                                                   False,
                                                   False]

        _: int
        regex_pair: str

        for _, regex_pair in enumerate(recursive_regex_input_list):
            self.assertEqual(recursive_regex(regex_pair),
                             recursive_regex_output_list[_])

    def test_unequal_strings_find_match(self):

        unequal_length_strings_input_list: List[str] = ["a|a",
                                                        "a|b",
                                                        "7|7",
                                                        "6|7",
                                                        ".|a",
                                                        "a|.",
                                                        "|a",
                                                        "|",
                                                        "a|",
                                                        "apple|apple",
                                                        ".pple|apple",
                                                        "appl.|apple",
                                                        ".....|apple",
                                                        "|apple",
                                                        "apple|",
                                                        "apple|peach",
                                                        "le|apple",
                                                        "app|apple",
                                                        "a|apple",
                                                        ".|apple",
                                                        "apwle|apple",
                                                        "peach|apple",
                                                        '^app|apple',
                                                        'le$|apple',
                                                        '^a|apple',
                                                        '.$|apple',
                                                        'apple$|tasty apple',
                                                        '^apple|apple pie',
                                                        '^apple$|apple',
                                                        '^apple$|tasty apple',
                                                        '^apple$|apple pie',
                                                        'app$|apple',
                                                        '^le|apple',
                                                        "colou?r|color",
                                                        "colou?r|colour",
                                                        "colou?r|colouur",
                                                        "colou*r|color",
                                                        "colou*r|colour",
                                                        "colou*r|colouur",
                                                        "colou+r|colour",
                                                        "colou+r|color",
                                                        ".*|aaa",
                                                        ".+|aaa",
                                                        ".?|aaa",
                                                        "no+$|noooooooope",
                                                        "^no+|noooooooope",
                                                        "^no+pe$|noooooooope",
                                                        "^n.+pe$|noooooooope",
                                                        "^n.+p$|noooooooope",
                                                        "\\.$|end.",
                                                        "3\\+3|3+3=6",
                                                        "\\?|Is this working?",
                                                        "\\\\|\\",
                                                        "colou\\?r|color",
                                                        "colou\\?r|colour"]

        unequal_length_strings_output_list: List[bool] = [True,
                                                          False,
                                                          True,
                                                          False,
                                                          True,
                                                          False,
                                                          True,
                                                          True,
                                                          False,
                                                          True,
                                                          True,
                                                          True,
                                                          True,
                                                          True,
                                                          False,
                                                          False,
                                                          True, True, True, True, False, False, True, True,
                                                          True, True, True, True, True, False, False, False,
                                                          False, True, True, False, True, True, True, True, False,
                                                          True, True, True, False, True, True, True, False, True,
                                                          True, True, True, False, False]

        _: int
        regex_pair: str

        for _, regex_pair in enumerate(unequal_length_strings_input_list):
            self.assertEqual(unequal_strings_find_match(regex_pair), unequal_length_strings_output_list[_])


if __name__ == '__main__':
    unittest.main()
