from typing import Tuple, List

from regex_engine import recursive_regex, unequal_strings_find_match


def find_previous_character_if_question_mark_metacharacter(regex: str, start: int) -> Tuple[int, str]:
    """
    Recursive function for finding the "?" metacharacter,
    it's index, the previous index and the symbol preceding the metacharacter.
    The "?" metacharacter means that the previous symbol should occur once or
    0 times.

    :param regex: The regex to find the metacharacter in it.
    Example: "colou?r".
    :type regex: str
    :param start: The index from which to search the metacharacter.
    Example: 0.
    :type start: int
    :returns: The tuple of 2 results - an index of the character previous to
    the "?" metacharacter and a character that is previous to the index - in
    the case it founds the metacharacter searched. Otherwise - it returns -1
    and "s" (a dummy sign), to prevent TypeError. Example: (4, "u").
    :rtype: Tuple[int, str]

    """
    # 0 or 1 times match

    i: int = regex.find("?", start)

    # If there is no "?", -1 and 's' - the 'dummy' character is returned.

    if i == -1:
        return -1, 's'

    # found the "?" metacharacter as the first character in the regex,
    # so it doesn't meet the criteria. Keep searching further.

    if i == 0:

        return find_previous_character_if_question_mark_metacharacter(regex, 1)

    # if the "?" metacharacter is found, the previous index and the character
    # with the previous index are returned.

    else:
        previous_char: str = regex[i - 1]
        return (i - 1), previous_char


def make_comparisons_question_mark(regex: str, input_str: str) -> Tuple[bool, int]:
    """
    Returns True if it finds 1 or 0 examples of the characters at the probed
    indexes. As it supports "." wildcard - the third block in the wildcard
    block should return False, but it returns True - because of the Jetbrains
    stage tests.

    :param regex: The regex string with "?" in it. Or not. Example: "colou?r".
    :type regex: str
    :param input_str: The string to be compared to the regex.
    Example: "colour".
    :type input_str: str
    :returns: A tuple - if there is a match (True or False) and a number of
     matched characters (0, 1 or 2). Example: (False, 0).
    :rtype: Tuple[bool, int]
    """

    index: int = find_previous_character_if_question_mark_metacharacter(regex, 0)[0]

    # No "?" metacharacter had been found, the False is returned.

    if index == -1:

        return False, 0

    # The "?" metacharacter had been found, so the previous_char (the previous
    # character to metacharacter is extracted.

    else:

        previous_char: str = find_previous_character_if_question_mark_metacharacter(regex, 0)[1]

        # The wildcard block.

        if previous_char == ".":

            # The chosen_char is the character in the input_str corresponding to the
            # regex character that was extracted by the
            # find_previous_character_if_question_mark_metacharacter function.

            chosen_char: str = input_str[index]

            # No character found, 0 matches - returns True.

            if not chosen_char:
                return True, 0

            # One match, returns True.

            elif chosen_char != input_str[index - 1]:
                return True, 1

            # Two matches (at least), returns True.

            elif chosen_char == input_str[index - 1]:
                return True, 2

            elif chosen_char == input_str[index + 1]:
                return True, 2

        # The "without a wildcard" block.

        else:

            if len(str(find_previous_character_if_question_mark_metacharacter(regex, 0))) == 1:

                return False, 0

            else:

                # Checking the outcomes, finding the matches, churning the answers.

                # Index has to be greater or equal to 1.

                if index >= 1:

                    # One match found, returns True.

                    if input_str[index] == previous_char:
                        return True, 1

                    elif input_str[index - 1] == previous_char \
                            and input_str[index - 2] != previous_char:

                        return True, 1

                    # More than 1 match found, returns False.

                    elif input_str[index - 1] == previous_char \
                            and input_str[index - 2] == previous_char:

                        return False, 0

                    elif input_str[index - 1] != previous_char:

                        return True, 0

                    elif input_str[index - 1] == previous_char \
                            and input_str[index] == previous_char:

                        return False, 0

                    elif input_str[index] == previous_char \
                            and input_str[index + 1] == previous_char:

                        return False, 0


def find_previous_character_if_question_mark_metacharacter_reversed(regex: str, start: int) \
        -> Tuple[int, str]:
    """
    Recursive function for finding the "?" metacharacter and the previous
    character in the regex adjusted for some end-string searching. The "?"
    metacharacter allows for 0 or 1 match.

    :param regex: The regex to be checked for the "?" metacharacter. May be
    already reversed. Example: "$?a"
    :type regex:str
    :param start: Index to start the search with. Example: 0.
    :type start: int
    :returns: A tuple: a index of a searched character after the metacharacter and
    a character at that index. Example: (2, "a")
    """
    # 0 or 1 times match

    # Finding the index of the metacharacter.

    i: int = regex.find("?", start)

    # no "?" found in the regex, -1 is returned altogether with a dummy string

    if i == -1:
        return -1, 's'

    # Found metacharacter is the first symbol in the word, it doesn't meet the
    # conditions. Keep searching further.

    else:
        next_char: str = regex[i + 1]
        return (i + 1), next_char

def make_comparisons_question_mark_reversed(regex: str, input_str: str) -> \
        Tuple[bool, int]:
    """
    Returns True if it finds 1 or 0 examples of the characters at the probed
    indexes with the "?" metacharacter and number of the matches. Supports a
    wildcard.

    :param regex: The regex to be probed. Example: "$?a".
    :type regex: str
    :param input_str: The string to be compared to the regex. Example: "a".
    :returns: A tuple - True if there is a match, False - if there is no match
     and a number of matches, if it applies. Example: (True, 0).
    :rtype: Tuple[bool, int]
    """

    index: int
    next_char: str
    index, next_char = find_previous_character_if_question_mark_metacharacter_reversed(regex, 0)

    # The wildcard block.

    if next_char == ".":

        chosen_char: str = input_str[index]

        # The chosen_char is an empty string, returns True. Zero matches.

        if not chosen_char:
            return True, 0
        # One match, returns True.

        elif chosen_char != input_str[index + 1]:
            return True, 1
        # 2 matches, wildcard case, returns True.

        elif chosen_char == input_str[index - 1]:
            return True, 2

        elif chosen_char == input_str[index + 1]:
            return True, 2

    # "No wildcard" block.

    else:

        if len(str(find_previous_character_if_question_mark_metacharacter_reversed(regex, 0))) == 1:
            return False, 0

        else:

            # 0 matches, returns True.
            if input_str[index-1] == next_char and input_str[index] != next_char:
                return True, 1

            elif input_str[index] != next_char:
                return True, 0

            # 1 match, returns True.
            elif input_str[index-1] == next_char and input_str[index] != next_char:
                return True, 1

            # 2 matches, returns False and 0.

            elif input_str[index-1] == next_char and input_str[index] == next_char:
                return False, 2

def find_previous_character_if_multiplication_mark_metacharacter(regex: str, start: int) -> \
        Tuple[int, str]:
    """
    Recursive function for finding the "*" metacharacter and the previous
    character in the regex. The "*" metacharacter allows for 0 or more
    matches. Supports wildcard character ".".

    :param regex: The regex to be searched for "*" metacharacter.
    Example: ".*".
    :type regex: str
    :param start: The start index of the regex to be searched for the
     metacharacter for. Example: 0.
    :type start: int
    :returns: A tuple: an index of the previous character to the metacharacter
     and that character.
    """
    # 0 or more times match

    # The i: index of the metacharacter.

    i: int = regex.find("*", start)

    # If there is no metacharacter in the given regex, -1 and a dummy
    # character is returned.

    if i == -1:
        return -1, 's'

    # The metacharacter is found at the first index that doesn't meet the
    # criteria, so the search is continued from the next index.

    if i == 0:
        return find_previous_character_if_multiplication_mark_metacharacter(regex, 1)

    # If index meets the criteria, the index of the character preceding
    # the metacharacter is returned and that character is also returned.

    else:
        previous_char: str = regex[i - 1]
        return (i - 1), previous_char


def make_comparisons_multiplication_mark(regex: str, input_str: str) -> \
        Tuple[bool, int]:
    """
    Returns True if it finds 0 or more times the preselected characters at the
    probed indexes. Supports wildcard "." character.

    :param regex: The regex to be compared to the input_str string. Example:
    ".*".
    :type regex: str
    :param input_str: The input string to be compared to the regex. Example:
    "aaa".
    :type input_str: str
    :returns: A tuple: if there is a match or no and the number of matches.
    :rtype: Tuple[bool, int]
    """

    index: int
    previous_char: str

    index, previous_char = find_previous_character_if_multiplication_mark_metacharacter(regex, 0)

    # The wildcard block.

    if previous_char == ".":

        # The chosen_char is a character that corresponds to the character at the
        # index extracted from the regex by the
        # find_previous_character_if_multiplication_mark_metacharacter function.
        if input_str:

            chosen_char: str = input_str[index]

            # 0 matches, returns True.
            if len(input_str) >= 2:

                if chosen_char == input_str[index - 1] \
                        or chosen_char == input_str[index + 1]:
                    return True, 2

                elif chosen_char != previous_char:
                    return True, 0

                # The chosen_char is an empty string, returns True.

                elif not chosen_char:
                    return True, 0

                # 2 matches, returns True.

                elif chosen_char == input_str[index - 1] \
                        or chosen_char == input_str[index + 1]:
                    return True, 2

                # 1 match, returns True.

                elif chosen_char != input_str[index - 1] \
                        or chosen_char != input_str[index + 1]:
                    return True, 1

            elif len(input_str)==1:

                if chosen_char:
                    return True, 1

                # The chosen_char is an empty string, returns True.

        elif not input_str:
            return True, 0

    else:

        # The " no wildcard" block.

        if len(str(find_previous_character_if_multiplication_mark_metacharacter(regex, 0))) == 1:
            return False, 0

        else:

            # 0 matches, returns True.

            if input_str[index] != previous_char:
                return True, 0

            # 1 match, returns True.

            elif input_str[index] == previous_char \
                    and input_str[index + 1] == previous_char:
                return True, 2

            elif input_str[index] == previous_char \
                    and input_str[index - 1] != previous_char:
                return True, 1

            # 2 matches, returns True.

            elif input_str[index] == previous_char \
                    and input_str[index - 1] == previous_char:
                return True, 2

def find_previous_character_if_addition_mark(regex: str, start: int) -> \
        Tuple[int, str]:
    """
    Recursive function for finding the "+" metacharacter and the previous
    character in the regex. The "+" metacharacter allows for 1 or more
    matches. Supports wildcard character ".".

    :param regex: The regex to be searched for "+". Example: ".+".
    :type regex: str
    :param start: The starting index from which the regex will be searched
     from. Example: 0.
    :type start: int
    """
    # 1 or more times match

    # i : the index at which the metacharacter is found.

    i: int = regex.find("+", start)

    if i == -1:  # there is no "+" metacharacter in the word
        return -1, 's'

    # Found the "+" metacharacter as the first symbol in the word, it doesn't
    # meet the conditions. Keep searching further.

    if i == 0:
        return find_previous_character_if_addition_mark(regex, 1)

    # The found index meets the conditions so the index previous to the
    # metacharacter is returned altogether with the character preceding
    # the metacharacter.

    else:
        previous_char: str = regex[i - 1]
        return (i - 1), previous_char

def make_comparisons_addition_mark(regex: str, input_str: str) -> \
        Tuple[bool, int]:
    """
    Returns True if it finds 1 or more examples of the characters at the
    probed indexes. Supports the wildcard "." symbol.

    :param regex: The regex to be compared to the input_str string. Example:
    ".+".
    :param input_str: The input string to be compared to the regex string.
    Example: "aaa".
    :returns: A tuple: True if there is a match, False otherwise and a
    number of matches. Example: (True, 2).
    :rtype: Tuple[bool, int].
    """
    index: int = find_previous_character_if_addition_mark(regex, 0)[0]
    previous_char: str = find_previous_character_if_addition_mark(regex, 0)[1]

    # The wildcard block.

    if previous_char == ".":

        # The chosen_char is the character corresponding to the character at the
        # the corresponding index in the regex.

        chosen_char: str = input_str[index]

        # If there is a chosen_char, return True. One match.

        if chosen_char == input_str[index - 1]:
            return True, 2

        elif chosen_char == input_str[index + 1]:
            return True, 2

        elif chosen_char:
            return True, 1

        # If the chosen_char matched 2 times, returns True.

        # If there is no chosen_char, return False.

        elif not chosen_char:
            return False, 0

    else:

        if len(str(find_previous_character_if_addition_mark(regex, 0))) == 1:
            return False, 0

        # The "no wildcard" block.

        else:

            # No match, return False.

            if input_str[index] != previous_char:
                return False, 0

            # 1 match, return True.

            elif input_str[index] == previous_char \
                    and input_str[index + 1] != previous_char:
                return True, 1

            elif input_str[index] == previous_char \
                    and input_str[index + 1] == previous_char:
                return True, 2

            elif input_str[index] == previous_char \
                    and input_str[index - 1] != previous_char:
                return True, 1

            # 2 matches, return True.

            elif input_str[index] == previous_char \
                    and input_str[index - 1] == previous_char:
                return True, 2

def find_next_character_if_backslash_detected_reversed(regex: str, start: int) \
        -> Tuple[int, str]:
    """
    Function for finding the "\\" character, it's index, the previous index and
    the symbol before the backslash.

    :param regex: The regex to find the backlash in it. May already be
     reversed. Example: "?//".
    :type regex: str
    :param start: The index from which to search the backslash. Example: 0.
    :type start: int
    :returns: A tuple of 2 results - an index of the character previous to the
    backslash metacharacter and a character with that index - in the case it
    founds the backslash. Otherwise - it returns -1 and "s" (a dummy
    character), to prevent TypeError.
    :rtype: Tuple[int, str]
    """

    # i: index of the backslash.

    i: int = regex.find("\\", start)

    # If there is no backslash detected, -1 and 's' (the 'dummy' character)
    # are returned.

    if i == -1:
        return -1, 's'

    elif i == 0:
        previous_char: str = regex[i + 1]
        return (i + 1), previous_char


    # if backslash metacharacter is found, the previous index and the
    # character with the previous index are returned.

    else:
        previous_char: str = regex[i - 1]
        return (i - 1), previous_char







